// App.js
import React, { useState } from 'react';
import { BrowserRouter as Router, Route, Routes, Navigate } from 'react-router-dom';
import Articles from './Articles';
import AdminLogin from './AdminLogin';

const App = () => {
  const [isAdmin, setIsAdmin] = useState(false);

  const handleLogin = () => {
    // Simulate authentication, set isAdmin to true for simplicity
    setIsAdmin(true);
  };

  // const handleLogout = () => {
  //   setIsAdmin(false);
  // };

  return (
    <Router>
      <Routes>
        <Route
          path="/"
          element={isAdmin ? <Navigate to="/articles" /> : <AdminLogin onLogin={handleLogin} />}
        />
        <Route
          path="/articles"
          element={isAdmin ? <Articles /> : <Navigate to="/" />}
        />
      </Routes>
    </Router>
  );
};

export default App;
