// Articles.js
import React, { useState } from 'react';

const Articles = () => {
    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const [image, setImage] = useState('');
    const [articles, setArticles] = useState([]);
    const [selectedImage, setSelectedImage] = useState(null);

    const handlePost = async () => {
        try {
            let imageBase64 = '';

            if (selectedImage) {
                // Extract base64 part without the prefix
                const prefixIndex = selectedImage.indexOf('base64,');
                if (prefixIndex !== -1) {
                    imageBase64 = selectedImage.slice(prefixIndex + 'base64,'.length);
                }
            }

            const token = localStorage.getItem('jwt');

            const response = await fetch('http://vivid-idea.com/api1/articles', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `${token}`
                },
                body: JSON.stringify({
                    title,
                    description,
                    imageData: imageBase64
                }),
            });

            if (!response.ok) {
                throw new Error('Failed to post article');
            }

            const newArticle = await response.json();
            setArticles((prevArticles) => [...prevArticles, { title, description, image, id: newArticle.id }]);

            // Clear form fields
            setTitle('');
            setDescription('');
            setImage('');
            setSelectedImage(null);
        } catch (error) {
            console.error('Error posting article:', error.message);
        }
    };

    const handleImageChange = async (e) => {
        const file = e.target.files[0];
        if (file) {
            try {
                const compressedImageBase64 = await compressImage(file);
                setSelectedImage(compressedImageBase64);
            } catch (error) {
                console.error('Error compressing image:', error.message);
            }
        }
    };

    const compressImage = (file) => {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.onload = (event) => {
                const img = new Image();
                img.src = event.target.result;

                img.onload = () => {
                    const canvas = document.createElement('canvas');
                    const maxImageSize = 800; // Adjust as needed

                    let width = img.width;
                    let height = img.height;

                    if (width > maxImageSize || height > maxImageSize) {
                        if (width > height) {
                            height *= maxImageSize / width;
                            width = maxImageSize;
                        } else {
                            width *= maxImageSize / height;
                            height = maxImageSize;
                        }
                    }

                    canvas.width = width;
                    canvas.height = height;

                    const ctx = canvas.getContext('2d');
                    ctx.drawImage(img, 0, 0, width, height);

                    // Use 'image/jpeg' for JPEG format, adjust quality as needed
                    const compressedImageBase64 = canvas.toDataURL('image/jpeg', 0.8);
                    resolve(compressedImageBase64);
                };

                img.onerror = (error) => {
                    reject(error);
                };
            };

            reader.readAsDataURL(file);
        });
    };

    // Assuming you have a function to remove the token when the user logs out
    const removeAuthToken = () => {
        localStorage.removeItem('jwt');
    };

    // Logout component or function
    const handleLogout = () => {
        // Call the function to remove the token
        removeAuthToken();
    };

    return (
        <div className="container mt-4">
            <h2 className="mb-4">Articles</h2>
            <button type="button" onClick={handleLogout}>
                Logout
            </button>

            <div className="mb-4">
                <h3>Post New Article</h3>
                <form>
                    <div className="mb-3">
                        <label htmlFor="title" className="form-label">
                            Title:
                        </label>
                        <input
                            type="text"
                            className="form-control"
                            id="title"
                            value={title}
                            onChange={(e) => setTitle(e.target.value)}
                        />
                    </div>
                    <div className="mb-3">
                        <label htmlFor="description" className="form-label">
                            Description:
                        </label>
                        <textarea
                            className="form-control"
                            id="description"
                            value={description}
                            onChange={(e) => setDescription(e.target.value)}
                        />
                    </div>
                    <div className="mb-3">
                        <label htmlFor="image" className="form-label">
                            Image Upload:
                        </label>
                        <input
                            type="file"
                            className="form-control"
                            id="image"
                            onChange={handleImageChange}
                        />
                    </div>
                    {selectedImage && (
                        <div className="mb-3">
                            <img src={selectedImage} alt="Selected" className="img-thumbnail" />
                        </div>
                    )}
                    <button type="button" className="btn btn-primary" onClick={handlePost}>
                        Post
                    </button>
                </form>
            </div>

            <div>
                <h3>Article List</h3>
                {articles.map((article, index) => (
                    <div key={index} className="card mb-3">
                        <div className="card-body">
                            <h4 className="card-title">{article.title}</h4>
                            <p className="card-text">{article.description}</p>
                            {article.image && (
                                <img src={article.image} alt={article.title} className="img-fluid" />
                            )}
                        </div>
                    </div>
                ))}
            </div>
        </div>
    );
};

export default Articles;
